package be.kdg.carproject.generics;

import java.util.Comparator;
import java.util.LinkedList;
import java.util.Map;
import java.util.TreeMap;

public class PriorityQueue<T> implements FIFOQueue<T>{

    private TreeMap<Integer,LinkedList<T>> map;

    public PriorityQueue(){
        map = new TreeMap<>(Comparator.reverseOrder());
    }

    @Override
    public boolean enqueue(T element, int priority) {

        for (Map.Entry<Integer,LinkedList<T>>  pair : map.entrySet()) {
            for (T t : pair.getValue()) {
                if (element == t) {
                    return false;
                }
            }
        }
        if (!map.containsKey(priority)) {
            map.put(priority, new LinkedList<T>());
        }
        map.get(priority).add(element);
        return true;
    }

    @Override
    public T dequeue() {
        T t = map.firstEntry().getValue().getFirst();
        map.firstEntry().getValue().removeFirst();
        if(map.firstEntry().getValue().isEmpty()){
            map.remove(map.firstKey());
        }
        return t;
    }

    @Override
    public int search(T element) {
        int positie = 0;
        for (Map.Entry<Integer,LinkedList<T>>  pair : map.entrySet()) {
            for (T t : pair.getValue()) {
                positie++;
                if (element == t) {
                    return positie;
                }
            }
        }
        return -1;
    }

    @Override
    public int getSize() {
        int size= 0;
        for (Map.Entry<Integer,LinkedList<T>>  pair : map.entrySet()) {
            size += pair.getValue().size();
        }
        return size;
    }

    @Override
    public String toString() {
        StringBuilder str = new StringBuilder();
        for (Map.Entry<Integer,LinkedList<T>>  pair : map.entrySet()) {
            for (T t : pair.getValue()) {
               str.append(pair.getKey()).append(": ").append(t.toString()).append("\n");
            }
        }
        return str.toString();
    }
}
