package be.kdg.carproject;

import be.kdg.carproject.data.Data;
import be.kdg.carproject.generics.PriorityQueue;
import be.kdg.carproject.model.Car;

import java.util.LinkedList;
import java.util.Map;
import java.util.Random;
import java.util.TreeMap;

public class Demo_2 {
    public static void main(String[] args) {
        PriorityQueue<String> myQueue = new PriorityQueue<>();
        myQueue.enqueue("Tokio",2);
        myQueue.enqueue("Denver",5);
        myQueue.enqueue("Rio",2);
        myQueue.enqueue("Oslo",3);
        System.out.println("Overzicht van de PriorityQueue:");
        System.out.println(myQueue.toString());
        System.out.println("aantal: "+myQueue.getSize());
        System.out.println("positie van Tokio: "+myQueue.search("Tokio"));
        System.out.println("positie van Nairobi: "+myQueue.search("Nairobi"));
        for(int i = 0;i< 4;i++){
            System.out.println("Dequeue: " + myQueue.dequeue());
        }System.out.println("Size na dequeue: "+myQueue.getSize());



        PriorityQueue<Car> carQueue = new PriorityQueue<>();
        Random ran = new Random();
        for (Car car : Data.getData()) {
            carQueue.enqueue(car,ran.nextInt(5));
        }
        System.out.println("Overzicht van de PriorityQueue:");
        System.out.println(myQueue.toString());
        System.out.println("aantal: "+myQueue.getSize());
        System.out.println("positie van Tokio: "+myQueue.search("Tokio"));
        System.out.println("positie van Nairobi: "+myQueue.search("Nairobi"));

    }

}
