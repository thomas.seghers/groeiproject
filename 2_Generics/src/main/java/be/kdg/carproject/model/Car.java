package be.kdg.carproject.model;

import java.time.LocalDate;
import java.util.Objects;

/**
 * Deze klasse omvat een auto met zijn belangrijkste kenmerken
 *
 * @author Thomas seghers
 * @version 1.1
 * @see <a href="https://nl.wikipedia.org/wiki/Auto">AutoSpec</a>
 */
public class Car implements Comparable<Car>{
    String name;
    double acceleration;
    int range;
    LocalDate productionDate;
    Brand brand;

    /**
     *Dit is de basis constructor van deze klasse indien alle parameter meegegeven zijn.
     * @param name De  naam van de auto
     * @param acceleration De tijd nodig voor het oprtekken van 0 tot 100km per uur
     * @param range de afstand die de auto kan afleggen met een volle tank
     * @param productionDate de datum van de eerste productie van het model
     * @param brand het merk van de auto
     */
    public Car(String name, double acceleration, int range, LocalDate productionDate, Brand brand) {
        setName(name);
        setAcceleration(acceleration);
        setRange(range);
        setProductionDate(productionDate);
        setBrand(brand);
    }

    /**
     * Dit is de default constructor wanner er geen parameters meegegeven zijn.
     */
    public Car (){
        this("Anoniem",19.0,1,LocalDate.of(1800,1,1),Brand.BMW);
    }

    /**
     * stand setter voor de naam
     * @param name naam van de auto
     */
    public void setName(String name) {
        if(!name.isEmpty()){
           this.name = name;
        }else {
            throw new IllegalArgumentException("Naam is empty") ;
        }

    }

    /**
     * standaard setter voor de acceleration
     * @param acceleration De tijd nodig voor het oprtekken van 0 tot 100km per uur
     */
    public void setAcceleration(double acceleration) {
        if (acceleration >0 && acceleration <20){
            this.acceleration = acceleration;
        }else {
            throw new IllegalArgumentException("Acceleration is not realistic") ;
        }

    }

    /**
     * standaard setter voor de range
     * @param range de afstand die de auto kan afleggen met een volle tank
     */
    public void setRange(int range) {
        if (range >0 && range <2000){
            this.range = range;
        }else {
            throw new IllegalArgumentException("the Action range is out of range");
        }

    }

    /**
     * standaard setter voor de productie datum
     * @param productionDate de datum van de eerste productie van het model
     */
    public void setProductionDate(LocalDate productionDate) {
        if (LocalDate.now().isAfter(productionDate)){
            this.productionDate = productionDate;
        }else {
            throw new IllegalArgumentException("No Realistic date");
        }

    }

    /**
     * standaard setter voor de productie datum
     * @param brand het merk van de auto
     * @see be.kdg.carproject.model.Brand
     */
    public void setBrand(Brand brand) {
        this.brand = brand;
    }

    /**
     *  getter voor de naam
     * @return de naam van de auto
     */
    public String getName() {
        return name;
    }

    /**
     * getter voor de acceleration
     * @return de acceleratie van de auto
     */
    public double getAcceleration() {
        return acceleration;
    }

    /**
     * getter voor de range
     * @return de range van de auto
     */
    public int getRange() {
        return range;
    }

    /**
     * getter voor de productionDate
     * @return de productie datum van de auto
     */
    public LocalDate getProductionDate() {
        return productionDate;
    }

    /**
     * getter voor de merk
     * @return het merk van de auto
     * @see be.kdg.carproject.model.Brand
     */
    public Brand getBrand() {
        return brand;
    }

    /**
     * Deze methode kijkt of dat het meeegeven object gelijk is aan het object zelf
     * @param o het ob
     * @return true of flase
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Car)) return false;
        Car car = (Car) o;
        return name.equals(car.name);
    }

    /**
     * Deze methode geeft de hashcode terug van de naam
     * @return hashcode van de naam
     */
    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    /**
     * deze methode vergelijkt op basis van de  naam
     * @param o  het auto object dat meegegven wordt
     * @return 2 of 1
     */
    @Override
    public int compareTo(Car o) {
        return this.name.compareTo(o.name) ;
    }

    /**
     * deze methode produseert een string met alle nuttige informatie van de klasse
     * @return een string met alle informatie
     */
    @Override
    public String toString() {
        return String.format("%-14s%-41s (°%4d)   Acceleration(0-100Km): %2.1f     range: %-4dKm",this.brand.toString(),this.name,this.productionDate.getYear(),acceleration, range);
    }
}


