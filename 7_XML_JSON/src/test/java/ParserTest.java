import be.kdg.carproject.data.Data;
import be.kdg.carproject.model.Cars;
import be.kdg.carproject.parsing.CarsDomParser;
import be.kdg.carproject.parsing.CarsGsonParser;
import be.kdg.carproject.parsing.CarsJaxbParser;
import be.kdg.carproject.parsing.CarsStaxParser;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class ParserTest {
    private final Cars cars = new Cars();

    @BeforeEach
    void setUp(){
        Data.getData().forEach(cars::add);
    }

    @Test
    void testStaxDom(){
        Assertions.assertDoesNotThrow(() -> new CarsStaxParser(cars, "datafiles/staxCars.xml"));
        final Cars[] carsList = new Cars[1];
        assertDoesNotThrow(() -> carsList[0] = CarsDomParser.domReadXml("datafiles/staxCars.xml"));
        assertEquals(cars.getCars(), carsList[0].getCars());
    }

    @Test
    void testJaxb(){
        assertDoesNotThrow(() -> CarsJaxbParser.JaxbWriteXml("datafiles/jaxbCars.xml", cars));
        final Cars[] carsList = new Cars[1];
        assertDoesNotThrow(() -> carsList[0] = CarsJaxbParser.JaxbReadXml("datafiles/jaxbCars.xml", Cars.class));
        assertEquals(cars.getCars(), carsList[0].getCars());
    }

    @Test
    void testGson(){
        assertDoesNotThrow(() -> CarsGsonParser.writeJson(cars, "datafiles/cars.json"));
        assertDoesNotThrow(() -> CarsGsonParser.readJson("datafiles/cars.json"));
        assertEquals(cars.getCars(), CarsGsonParser.readJson("datafiles/cars.json").getCars());
    }
}
