package be.kdg.carproject.parsing;

import be.kdg.carproject.model.Cars;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class CarsGsonParser {
    static GsonBuilder builder = new GsonBuilder().setPrettyPrinting();
    static Gson gson = builder.create();

    public static void writeJson(Cars cars, String file){

        try(FileWriter jsonWriter = new FileWriter(file)){
            jsonWriter.write(gson.toJson(cars));
        } catch (IOException e){
            e.printStackTrace();
        }
    }

    public static Cars readJson(String file){

        try (BufferedReader data = new BufferedReader(new FileReader(file))){
            return gson.fromJson(data, Cars.class);
        } catch (IOException e){
            e.printStackTrace();
            return null;
        }
    }
}
