package be.kdg.carproject.parsing;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import java.time.LocalDate;

public class LocalDateAdapter extends XmlAdapter<String, LocalDate> {
    public LocalDate unmarshal(String myString) throws Exception {
        return LocalDate.parse(myString);
    }

    public String marshal(LocalDate myDate) throws Exception {
        return myDate.toString();
    }
}
