package be.kdg.carproject.parsing;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;

public class CarsJaxbParser {
    public static void JaxbWriteXml(String file, Object root){
        JAXBContext context;
        try{
            context = JAXBContext.newInstance(root.getClass());
            Marshaller mar = context.createMarshaller();
            mar.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            mar.marshal(root, new File(file));
        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }

    public static <T> T JaxbReadXml(String file, Class<T> typeParameterClass){
        try{
            JAXBContext context = JAXBContext.newInstance(typeParameterClass);
            return (T) context.createUnmarshaller().unmarshal(new FileReader(file));
        }
        catch(JAXBException | FileNotFoundException e){
            e.printStackTrace();
        }
        return null;
    }
}
