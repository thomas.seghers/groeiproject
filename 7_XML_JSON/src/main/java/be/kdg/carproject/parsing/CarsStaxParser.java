package be.kdg.carproject.parsing;

import be.kdg.carproject.model.Car;
import be.kdg.carproject.model.Cars;
import com.sun.xml.txw2.output.IndentingXMLStreamWriter;

import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

public class CarsStaxParser {
    private XMLStreamWriter writer;
    private Cars cars;

    public CarsStaxParser(Cars cars, String filePath) {
        this.cars = cars;
        try {
            FileWriter file = new FileWriter(filePath,
                    StandardCharsets.UTF_8);
            writer = XMLOutputFactory.newInstance()
                    .createXMLStreamWriter(file);
            writer = new IndentingXMLStreamWriter(writer);
            staxWriteXML();
            writer.close();
        } catch (XMLStreamException | IOException e) {
            e.printStackTrace();
        }
    }

    private void staxWriteXML() {
        try {
            writer.writeStartDocument();
            writer.writeStartElement("cars");
            cars.getCars().forEach(this::writeElement);
            writer.writeEndElement();
            writer.writeEndDocument();
        } catch (XMLStreamException e) {
            e.printStackTrace();
        }
    }

    private void writeElement(Car car) {
        try {
            writer.writeStartElement("car");
            writer.writeAttribute("name", car.getName());

            writer.writeStartElement("acceleration");
            writer.writeCharacters(Double.toString(car.getAcceleration()));
            writer.writeEndElement();

            writer.writeStartElement("range");
            writer.writeCharacters(Integer.toString(car.getRange()));
            writer.writeEndElement();


            writer.writeStartElement("productionDate");
            writer.writeCharacters(car.getProductionDate().toString());
            writer.writeEndElement();

            writer.writeStartElement("brand");
            writer.writeCharacters(car.getBrand().name());
            writer.writeEndElement();

            writer.writeEndElement();
        } catch (XMLStreamException e) {
            e.printStackTrace();
        }
    }
}
