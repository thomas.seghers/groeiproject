package be.kdg.carproject.parsing;

import be.kdg.carproject.data.Data;
import be.kdg.carproject.model.Brand;
import be.kdg.carproject.model.Car;
import be.kdg.carproject.model.Cars;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.IntStream;

public class CarsDomParser {
    static List<Car> dataList = Data.getData();


    public static Cars domReadXml(String fileName){
        Cars cars = new Cars();
        try{
            DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            Document doc = builder.parse(new File(fileName));
            doc.getDocumentElement().normalize();
            Element rootElement = doc.getDocumentElement();
            NodeList carsNodes = rootElement.getChildNodes();

            IntStream.range(0, carsNodes.getLength()).filter(i -> carsNodes.item(i).getNodeType() == Node.ELEMENT_NODE).mapToObj(i -> (Element) carsNodes.item(i)).map(e -> new Car(
                    e.getAttribute("name"),
                    Double.parseDouble(e.getElementsByTagName("acceleration").item(0).getTextContent()),
                    Integer.parseInt(e.getElementsByTagName("range").item(0).getTextContent()),
                    LocalDate.parse(e.getElementsByTagName("productionDate").item(0).getTextContent()),
                    Brand.valueOf(e.getElementsByTagName("brand").item(0).getTextContent())
            )).forEach(cars::add);
            return cars;
        }
        catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
