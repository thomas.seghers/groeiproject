package be.kdg;


import be.kdg.carproject.data.Data;
import be.kdg.carproject.model.Brand;
import be.kdg.carproject.model.Car;
import be.kdg.carproject.model.Cars;

import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDate;
import java.util.List;
import java.util.logging.LogManager;

public class Demo_5 {
    public static void main(String[] args) {
        loadLoggingConfiguration();
        Cars cars = new Cars();
        List<Car> rijkList = Data.getData();
        rijkList.forEach(cars::add);
        System.out.println(cars.remove("E Class All Terrain (W213) 220 d 4MATIC") ? "Car verwijdert" : "Car niet verwijdert");

        Car test1 = new Car("E Class All Terrain (W213) 220 d 4MATIC",8.0,1269 , LocalDate.of(2050,1,1), Brand.MERCEDES);
        Car test2 = new Car("", 8.0,1269 , LocalDate.of(2016,1,1), Brand.MERCEDES);
        Car test3 = new Car("E Class All Terrain (W213) 220 d 4MATIC",8.0,-5 , LocalDate.of(2016,1,1), Brand.MERCEDES);

    }

    private static void loadLoggingConfiguration() {
        InputStream inputStream = Demo_5.class.getResourceAsStream("/logging.properties");
        try {
            LogManager.getLogManager().readConfiguration(inputStream);
        } catch(IOException e) {
            System.err.println("Logging configuratiebestand kan niet geladen worden");
        }

    }
}
