package be.kdg.carproject.model;

import java.time.LocalDate;
import java.util.Objects;
import java.util.logging.Logger;

public class Car implements Comparable<Car>{
    String name;
    double acceleration;
    int range;
    LocalDate productionDate;
    Brand brand;
    private final Logger logger = Logger.getLogger(Car.class.getName());

    public Car(String name, double acceleration, int doorCount, LocalDate productionDate, Brand brand) {
        setName(name);
        setAcceleration(acceleration);
        setRange(doorCount);
        setProductionDate(productionDate);
        setBrand(brand);
    }

    public Car (){
        this("Anoniem",19.0,1,LocalDate.of(1800,1,1),Brand.BMW);
    }

    public void setName(String name) {
        if(!name.isEmpty()){
           this.name = name;
        }else {
            logger.severe("Naam moet ingevuld zijn");;
        }

    }

    public void setAcceleration(double acceleration) {
        if (acceleration >0 && acceleration <20){
            this.acceleration = acceleration;
        }else {
            logger.severe( acceleration + " valt niet in de acceleration range (0-20)");

        }

    }

    public void setRange(int range) {
        if (range >0 && range <2000){
            this.range = range;
        }else {
            logger.severe("Het bereik moet tussen 0 en 2000 liggen, "+range+" valt daar buiten");
        }

    }

    public void setProductionDate(LocalDate productionDate) {
        if (LocalDate.now().isAfter(productionDate)){
            this.productionDate = productionDate;
        }else {
            logger.severe("De production date moet in het verleden zijn, " + productionDate + " is niet voor vandaag");
        }

    }

    public void setBrand(Brand brand) {
        this.brand = brand;
    }

    public String getName() {
        return name;
    }

    public double getAcceleration() {
        return acceleration;
    }

    public int getRange() {
        return range;
    }

    public LocalDate getProductionDate() {
        return productionDate;
    }

    public Brand getBrand() {
        return brand;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Car)) return false;
        Car car = (Car) o;
        return name.equals(car.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    @Override
    public int compareTo(Car o) {
        return this.name.compareTo(o.name) ;
    }

    @Override
    public String toString() {
        return String.format("%-14s%-41s (°%4d)   Acceleration(0-100Km): %2.1f     range: %-4dKm",this.brand.toString(),this.name,this.productionDate.getYear(),acceleration, range);
    }
}


