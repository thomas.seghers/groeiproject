package be.kdg.carproject.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class CarTest {


    private Car car1;
    private Car car2;

    @BeforeEach
    void setUp() {
        car1 = new Car("Car1", 6.8,840, LocalDate.of(2014,1,1), Brand.JAGUAR);
        car2 = new Car("Car2", 7.0,986, LocalDate.of(2015,1,1), Brand.JAGUAR);
    }

    @Test
    void testEquals() {
        assertNotEquals(car1, car2, "De 2 auto's mogen niet hetzelfde zijn");
        Car car3 = new Car("Car3", 4.3,652, LocalDate.of(2021,1,1), Brand.MERCEDES);
        assertNotEquals(car1, car3, "De naam van de auto moet uniek zijn");
    }

    @Test
    public void testIllegalDate() {
        assertThrows(IllegalArgumentException.class, () -> car1.setProductionDate(LocalDate.of(2050,1,1)), "Datum kan niet van na het huidige jaar zijn!");
    }

    @Test
    public void testLegalDate() {
        assertDoesNotThrow(() -> car1.setProductionDate(LocalDate.of(1998, 11, 12)), "Auto moet een geldige datum van voor de huidige datum hebben!");
    }

    @Test
    public void testCompareTo() {
        assertEquals(-1, car1.compareTo(car2),"auto naam van car1 moet voor car2 komen");
    }

    @Test
    public void testAcceleration() {
        assertEquals(7.0, car2.getAcceleration(), "De versnelling van car2 moet 7.0 zijn");
    }
}
