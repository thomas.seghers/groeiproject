package be.kdg.carproject.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class CarsTest {
    private Car car1;
    private Car car2;
    private Car car3;
    private Car car4;
    private Car car5;
    private Cars cars;

    @BeforeEach
    void setUp() {
        car1 = new Car("Car1", 6.7,765 , LocalDate.of(2019,1,1), Brand.TESLA);
        car2 = new Car("Car2",5.1 ,701 , LocalDate.of(2019,1,1), Brand.BMW);
        car3 = new Car("Car3", 4.2, 542, LocalDate.of(2021,1,1), Brand.MERCEDES);
        car4 = new Car("Car4",3.7 ,507 , LocalDate.of(2008,1,1), Brand.JAGUAR);
        car5 = new Car("Car5",3.2, 450, LocalDate.of(2018,1,1), Brand.JAGUAR);
        cars = new Cars();
        cars.add(car1);
        cars.add(car2);
        cars.add(car3);
        cars.add(car4);
        cars.add(car5);
    }

    @Test
    void testAdd() {
        assertTrue(cars.add(new Car("Car6", 3.7, 619, LocalDate.of(2017,1,1), Brand.JAGUAR)), "De auto moet uniek zijn");
    }

    @Test
    void testRemove() {
        assertTrue(cars.remove("car5"), "Deze auto bestaat niet");
        //int befoureCount = cars.getSize();
        //cars.remove("car5");
        //int afterCount = cars.getSize();
        //assertEquals(befoureCount, afterCount, "Car is niet verwijdert");
    }

    @Test
    public void testSort(){
        List<Car> carList = cars.sortedBy(Car::getRange);
        assertAll(
                () -> assertEquals(car5, carList.get(0)),
                () -> assertEquals(car4, carList.get(1)),
                () -> assertEquals(car3, carList.get(2)),
                () -> assertEquals(car2, carList.get(3)),
                () -> assertEquals(car1, carList.get(4))
        );
    }

    @Test
    public void testSortByCrit(){
        List<Car> carList = cars.sortedBy(Car::getAcceleration);
        assertArrayEquals(new Car[]{car5, car4, car3, car2, car1}, carList.toArray());
    }
}
