package be.kdg.carproject.util;

import java.util.List;
import java.util.OptionalDouble;
import java.util.function.Predicate;
import java.util.function.ToDoubleFunction;
import java.util.stream.Collectors;

public class CarFunctions {

    public static <T> List<T> filteredList(List<T> carList, Predicate<T> predicate){
        List<T> filterList = carList.stream().filter(predicate::test).collect(Collectors.toList());
        return filterList;
    }

    public static <T> Double average (List<T> carList, ToDoubleFunction<T> mapper) {
        OptionalDouble gemiddelde = carList.stream()
                .mapToDouble(mapper)
                .average();
        return gemiddelde.getAsDouble();
    }


    public static <T> long countIf(List<T> carList, Predicate<T> predicate){
        long count = carList.stream().filter(predicate::test).count();
        return count;
    }
}
