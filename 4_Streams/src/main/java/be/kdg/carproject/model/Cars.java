package be.kdg.carproject.model;

import java.util.*;
import java.util.function.Function;

public class Cars {
    TreeSet<Car> cars;

    public Cars() {
        this.cars = new TreeSet<Car>();
    }

    public boolean add(Car car) {
        if (!cars.isEmpty()) {
            for (Car o : cars) {
                if (car.equals(o)) {
                    return false;
                }
            }
        }
        cars.add(car);
        return true;
    }

    //    public void remove(String name){
//        Iterator<Car> it = cars.iterator();
//        while (it.hasNext()) {
//            if(it.next().getName() == name){
//                it.remove();
//            }
//        }
//    }

    // indien nodig terug naar iteratie methode gaan
    public boolean remove(String naam) {
        cars.removeIf(a -> a.getName().equalsIgnoreCase(naam));
        return cars.stream().findFirst().filter(a -> !a.getName().equals(naam)).isPresent();
    }

    public Car search(String name) {
        for (Car o : cars) {
            if (o.name.equals(name)) {
                return o;
            }
        }
        return null;
    }

//    public List<Car> sortedOnName(){
//        List<Car> sortedList = new ArrayList<>(cars);
//        sortedList.sort(Car::compareTo);
//        return sortedList;
//    }
//
//    public List<Car> sortedOnDate(){
//        List<Car> sortedList = new ArrayList<>(cars);
//        sortedList.sort(new Comparator<Car>() {
//            @Override
//            public int compare(Car o1, Car o2) {
//                return o1.productionDate.compareTo(o2.productionDate);
//            }
//        });
//        return sortedList;
//    }
//
//    public List<Car> sortedOnAcceleration(){
//        List<Car> sortedList = new ArrayList<>(cars);
//        sortedList.sort(new Comparator<Car>() {
//            @Override
//            public int compare(Car o1, Car o2) {
//                return Double.compare(o1.acceleration,o2.acceleration);
//            }
//        });
//        return sortedList;
//    }

    public List<Car> sortedBy(Function<Car, Comparable> function) {
        List<Car> sortedCar = new ArrayList<Car>(cars);
        sortedCar.sort(Comparator.comparing(function));
        return sortedCar;
    }

    public int getSize() {
        return cars.size();
    }
}
