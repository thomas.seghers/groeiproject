package be.kdg.carproject;

import be.kdg.carproject.data.Data;
import be.kdg.carproject.model.Brand;
import be.kdg.carproject.model.Car;
import be.kdg.carproject.model.Cars;
import be.kdg.carproject.util.CarFunctions;

import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.groupingBy;

public class Demo_4 {
    public static void main(String[] args) {

        System.out.println("Filtered on acceleration\n=========================================");
        List<Car> carList = Data.getData();
        carList = CarFunctions.filteredList(carList, car -> car.getAcceleration() > 6);
        carList.forEach(System.out::println);


        System.out.println("Filtered on Brand\n=========================================");
        carList = Data.getData();
        carList = CarFunctions.filteredList(carList, car -> car.getBrand().equals(Brand.BMW));
        carList.forEach(System.out::println);

        System.out.println("Filtered on range\n=========================================");
        carList = Data.getData();
        carList = CarFunctions.filteredList(carList, car -> car.getRange() < 600);
        carList.forEach(System.out::println);
        System.out.println("=========================================");


        carList = Data.getData();

        System.out.printf("Gemiddelde optreksnelheid: %.1f s\n",
                CarFunctions.average(carList, Car::getAcceleration));
        System.out.printf("Gemiddelde bereik: %.1f km\n",
                CarFunctions.average(carList, Car::getRange));

        System.out.printf("Aantal Cars met versnelling < 6: %d\n",
                CarFunctions.countIf(carList, car -> car.getAcceleration() < 50.0));
        System.out.printf("Aantal Cars met in de naam '1': %d\n",
                CarFunctions.countIf(carList, car -> car.getName().contains("1")));

        System.out.println("===========================");
        carList = Data.getData();

        System.out.print("Aantal auto's dat voor het jaar 2018 zijn geproduceert: ");
        System.out.println(carList.stream()
                .filter(e -> e.getProductionDate().isBefore(LocalDate.parse("2018-01-01")))
                .count());
        System.out.println("Alle auto's gesorteerd op merk en vervolgens op naam");
        System.out.println(carList.stream()
                .sorted(Comparator.comparing(Car::getBrand).thenComparing(Car::getName))
                .collect(groupingBy(Car::getBrand)));

        System.out.println("Alle auto's in hoofdletters, omgekeerd gesorteerd en zonder dubbels:");
        System.out.println(carList.stream()
                .map(r -> r.getName().toUpperCase())
                .distinct()
                .sorted(Comparator.reverseOrder())
                .collect(Collectors.joining(" , ")
                ));

        System.out.println("\nEen willekeurig auto met een range boven de 600:");
        Optional<Car> gezocht = CarFunctions.filteredList(carList, car -> car.getRange() > 600).stream().findAny();
        System.out.printf("%s: %d KM\n",
                gezocht.get().getName(),
                gezocht.get().getRange()
        );

        System.out.println("De snelste auto: ");
        System.out.println(carList.stream()
                .min(Comparator.comparing(Car::getAcceleration))
                .get()
                .getName());
        System.out.println("De auto met het grootse bereik: ");
        System.out.println(carList.stream()
                .max(Comparator.comparing(Car::getRange))
                .get()
                .getName());
        System.out.println("List met gesorteerde auto namen die eindigen met 'e':");
        System.out.println(carList.stream()
                .filter(e -> e.getName().endsWith("e"))
                .map(r -> r.getName().toUpperCase())
                .sorted()
                .collect(Collectors.toList())
        );

        Map<Boolean, List<Car>> carSplitOnDate = carList
                .stream()
                .sorted(Comparator.comparing(Car::getProductionDate))
                .collect(Collectors.partitioningBy(car -> car.getProductionDate().isAfter(LocalDate.of(2018, 1, 1))));
        System.out.println("Auto's voor 2018:");
        carSplitOnDate.get(false).forEach(System.out::println);
        System.out.println();
        System.out.println("Auto's na 2018:");
        carSplitOnDate.get(true).forEach(System.out::println);

        System.out.println("Alle Auto's gegroepeerd per Merk:");
        Map<Boolean, List<Car>> partitionedPoly =
                carList.stream().collect(
                        Collectors.partitioningBy(r -> r.getBrand().equals(Brand.BMW)));
        System.out.println("");
        List<Car> bmwCars = partitionedPoly.get(true);
        Collections.sort(bmwCars);
        System.out.print("BMW: ");
        bmwCars.stream().map(car -> car.getName() + ", ").forEach(System.out::print);
        System.out.println("");


        Map<Boolean, List<Car>> partitionedTesla =
                carList.stream().collect(
                        Collectors.partitioningBy(r -> r.getBrand().equals(Brand.TESLA)));
        System.out.println("");
        List<Car> teslaCars = partitionedTesla.get(true);
        Collections.sort(teslaCars);
        System.out.print("TESLA: ");
        teslaCars.stream().map(car -> car.getName() + ", ").forEach(System.out::print);
        System.out.println("");

        Map<Boolean, List<Car>> partitionedAudi =
                carList.stream().collect(
                        Collectors.partitioningBy(r -> r.getBrand().equals(Brand.AUDI)));
        System.out.println("");
        List<Car> AudiCars = partitionedAudi.get(true);
        Collections.sort(AudiCars);
        System.out.print("AUDI: ");
        AudiCars.stream().map(car -> car.getName() + ", ").forEach(System.out::print);
        System.out.println("");

        Map<Boolean, List<Car>> partitionedMercedes =
                carList.stream().collect(
                        Collectors.partitioningBy(r -> r.getBrand().equals(Brand.MERCEDES)));
        System.out.println("");
        List<Car> MercedesCars = partitionedMercedes.get(true);
        Collections.sort(MercedesCars);
        System.out.print("MERCEDES: ");
        MercedesCars.stream().map(car -> car.getName() + ", ").forEach(System.out::print);
        System.out.println("");

        Map<Boolean, List<Car>> partitionedJaguar =
                carList.stream().collect(
                        Collectors.partitioningBy(r -> r.getBrand().equals(Brand.JAGUAR)));
        System.out.println("");
        List<Car> JaguarCars = partitionedJaguar.get(true);
        Collections.sort(JaguarCars);
        System.out.print("JAGUAR: ");
        JaguarCars.stream().map(car -> car.getName() + ", ").forEach(System.out::print);
        System.out.println("");
    }
}
