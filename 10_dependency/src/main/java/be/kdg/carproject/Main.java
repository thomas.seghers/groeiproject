package be.kdg.carproject;

import be.kdg.carproject.database.CarDao;
import be.kdg.carproject.database.CarDbDao;
import be.kdg.carproject.service.CarsService;
import be.kdg.carproject.service.CarsServiceImpl;
import be.kdg.carproject.view.CarsPresenter;
import be.kdg.carproject.view.CarsView;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.util.logging.Logger;

// run enkel deze module met :
// ./gradlew run -p 10_dependency

public class Main  extends Application {
    private static final Logger L = Logger.getLogger(Main.class.getName());
    public static void main(String[] args) {
        L.info("Starting Car Management System on thread: " + Thread.currentThread().getName());
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        L.info("Running start methode on thread: " + Thread.currentThread().getName());
        CarDao rijkDao = CarDbDao.getInstance("db/cars");
        CarsView carsView = new CarsView();
        CarsService carsService = new CarsServiceImpl(rijkDao);
        new CarsPresenter(carsView, carsService);

        primaryStage.setScene(new Scene(carsView));
        primaryStage.show();

    }
}
