package be.kdg.carproject.service;

import be.kdg.carproject.database.CarDao;
import be.kdg.carproject.database.CarDbDao;
import be.kdg.carproject.exeptions.CarException;
import be.kdg.carproject.model.Car;

import java.util.List;

public class CarsServiceImpl implements  CarsService{
    private final CarDbDao carDbDao;

    public CarsServiceImpl(CarDao carDao) {
        carDbDao = CarDbDao.getInstance("database/cars");
    }

    @Override
    public List<Car> getAllCars() throws CarException {
        return carDbDao.getAllCars();
    }

    @Override
    public void addCar (Car car) throws CarException {
        carDbDao.insert(car);
    }
}
