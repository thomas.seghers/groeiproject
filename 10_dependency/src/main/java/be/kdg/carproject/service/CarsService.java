package be.kdg.carproject.service;

import be.kdg.carproject.model.Car;

import java.util.List;

public interface CarsService {
    public List<Car> getAllCars();
    public void addCar(Car rijk);
}
