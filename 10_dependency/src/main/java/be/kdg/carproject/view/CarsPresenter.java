package be.kdg.carproject.view;

import be.kdg.carproject.exeptions.CarException;
import be.kdg.carproject.model.Car;
import be.kdg.carproject.service.CarsService;
import javafx.collections.FXCollections;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;

import java.util.logging.Logger;

public class CarsPresenter {
    private static final Logger L = Logger.getLogger(CarsPresenter.class.getName());
    private final CarsView carsView;
    private final CarsService carsService;


    public CarsPresenter(CarsView carsView, CarsService carsService) {
        this.carsView = carsView;
        this.carsService = carsService;
        loadCars();
        addEventHandlers();
    }

    private void addEventHandlers(){
        carsView.getBtnSave().setOnAction(event -> {
            try{
                Car car = new Car(
                        carsView.getTfName().getText(),
                        carsView.getDpProductionDate().getValue(),
                        Integer.parseInt(carsView.getTfRange().getText()));
                carsService.addCar(car);
            }
            catch (CarException | IllegalArgumentException e){
                Alert alert = new Alert(Alert.AlertType.WARNING, e.getMessage(), ButtonType.OK);
                alert.showAndWait();
            }
            loadCars();
        });
    }

    private void loadCars(){
        try{
            carsView.getTvCars().setItems(FXCollections.observableList(carsService.getAllCars()));
        }
        catch (CarException | IllegalArgumentException e){
            Alert alert = new Alert(Alert.AlertType.WARNING, e.getMessage(), ButtonType.OK);
            alert.showAndWait();
        }

    }
}
