package be.kdg.carproject.view;


import be.kdg.carproject.model.Car;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;

import java.time.LocalDate;

public class CarsView  extends BorderPane{
    private TableView tvCars;
    private TextField tfName;
    private TextField tfRange;
    private DatePicker dpProductionDate;
    private Button btnSave;

    @SuppressWarnings("unchecked")
    public CarsView()  {
        tvCars = new TableView<>();
        TableColumn<Car, String> column1 = new TableColumn<>("Name");
        column1.setCellValueFactory(new PropertyValueFactory<>("name"));
        TableColumn<Car, LocalDate> column2 = new TableColumn<>("ProductionDate");
        column2.setCellValueFactory(new PropertyValueFactory<>("ProductionDate"));
        TableColumn<Car, String> column3 = new TableColumn<>("Range");
        column3.setCellValueFactory(new PropertyValueFactory<>("range"));


        tvCars.getColumns().add(column1);
        tvCars.getColumns().add(column2);
        tvCars.getColumns().add(column3);

        setCenter(tvCars);
        BorderPane.setMargin(tvCars, new javafx.geometry.Insets(10));

        tfName = new TextField();
        tfName.setPromptText("Name");
        dpProductionDate = new DatePicker();
        dpProductionDate.setPromptText("ProductionDate");
        tfRange = new TextField();
        tfRange.setPromptText("Range");
        btnSave = new Button("Save");

        HBox hbox = new HBox(tfName, dpProductionDate, tfRange, btnSave);
        BorderPane.setMargin(hbox, new javafx.geometry.Insets(10));
        hbox.setSpacing(5);
        this.setBottom(hbox);
    }

    TableView<Car> getTvCars() {
        return tvCars;
    }

    TextField getTfName() {
        return tfName;
    }

    TextField getTfRange() {
        return tfRange;
    }

    Button getBtnSave() {
        return btnSave;
    }

    DatePicker getDpProductionDate() {
        return dpProductionDate;
    }
}
