import be.kdg.carproject.data.Data;
import be.kdg.carproject.model.Car;
import be.kdg.carproject.database.CarDbDao;
import org.junit.jupiter.api.*;

import java.util.Comparator;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class CarDbDaoTest {
    private static CarDbDao database;

    @BeforeAll
    static void beforeAll(){
        database = new CarDbDao("db/cars");
    }

    @AfterAll
    static void afterAll(){
        database.close();
    }


    @AfterEach
    void afterEach(){
        database.delete("*");
    }

    @BeforeEach
    void setUp() {
        Data.getData().forEach(database::insert);
    }

    @Test
    public void testInsert(){
        assertTrue(database.insert(Data.getData().get(5)), "Database could not insert items");
    }

    @Test
    public void testRetrieve(){
        assertNotNull(database.retrieve("F-Pace 2021 P400"));
    }

    @Test
    public void testUpdate(){
        Car updatedCar = database.retrieve("i4 eDrive40");
        assertNotNull(updatedCar);
        updatedCar.setRange(689);
        assertTrue(database.update(updatedCar));
        assertEquals(689, database.retrieve("i4 eDrive40").getRange());
    }
    @Test
    public void testSortedOn(){
        List<Car> carsDb = database.sortedOn("SELECT * FROM cars ORDER BY naam");
        List<Car> carsList = Data.getData();
        carsList.sort(Comparator.comparing(Car::getName));
        assertEquals(carsList, carsDb);
    }

    @Test
    public void testDelete(){
        Car carToRemove = database.retrieve("F-Pace 2021 P400");
        assertNotNull(carToRemove);
        assertTrue(database.delete("F-Pace 2021 P400"));
        carToRemove = database.retrieve("F-Pace 2021 P400");
        assertNull(carToRemove);
    }


}
