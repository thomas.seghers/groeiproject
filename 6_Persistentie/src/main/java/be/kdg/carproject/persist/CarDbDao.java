package be.kdg.carproject.persist;


import be.kdg.carproject.model.Brand;
import be.kdg.carproject.model.Car;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

public class CarDbDao implements CarDao {
    private Connection connection;
    private final Logger logger = Logger.getLogger(CarDbDao.class.getName());

    public CarDbDao(String databasePad) {
        try {
            connection = DriverManager.getConnection("jdbc:hsqldb:file:" + databasePad, "sa", "");
            createTable();
        } catch (SQLException throwables) {
            logger.severe(throwables.getMessage());
        }
    }

    public void close() {
        if (connection != null) {
            try {
                connection.close();
            } catch (SQLException throwables) {
                logger.severe(throwables.getMessage());
            }
        }
    }

    private void createTable() {
        try {
            Statement statement = connection.createStatement();
            statement.execute("DROP TABLE cars IF EXISTS");
            String query = "CREATE TABLE cars (" +
                    "id INTEGER NOT NULL IDENTITY," +
                    "naam VARCHAR(50)," +
                    "acceleration DECIMAL(10,2)," +
                    "ranges INTEGER," +
                    "productionDate DATE," +
                    "brand VARCHAR(15))";
            statement.execute(query);
            statement.close();
        } catch (SQLException throwables) {
            logger.severe(throwables.getMessage());
        }
    }

    @Override
    public boolean insert(Car car) {
        try {
            String statement = "INSERT INTO cars(id, naam, acceleration, ranges, productionDate, brand) VALUES(null,?,?,?,?,?)";
            PreparedStatement prep = connection.prepareStatement(statement);

            prep.setString(1, car.getName());
            prep.setDouble(2, car.getAcceleration());
            prep.setInt(3, car.getRange());
            prep.setDate(4, Date.valueOf(car.getProductionDate()));
            prep.setString(5, car.getBrand().name());

            boolean status = prep.executeUpdate() > 0;
            prep.close();
            return status;
        } catch (SQLException throwables) {
            logger.severe(throwables.getMessage());
            return false;
        }
    }

    @Override
    public boolean delete(String naam) {
        String statement;
        try {
            PreparedStatement prep;
            if (naam.equals("*")) {
                statement = "TRUNCATE TABLE cars";
                prep = connection.prepareStatement(statement);
            } else {
                statement = "DELETE FROM cars WHERE naam = ?";
                prep = connection.prepareStatement(statement);
                prep.setString(1, naam);
            }
            boolean status = prep.executeUpdate() > 0;
            prep.close();
            return status;
        } catch (SQLException throwables) {
            logger.severe(throwables.getMessage());
            return false;
        }
    }

    @Override
    public boolean update(Car car) {
        try {
            String statement = "UPDATE cars SET " +
                    "naam = ?, " +
                    "acceleration = ?, " +
                    "ranges = ?, " +
                    "productionDate = ?, " +
                    "brand = ? " +
                    "WHERE id = ?";
            PreparedStatement prep = connection.prepareStatement(statement);
            prep.setString(1, car.getName());
            prep.setDouble(2, car.getAcceleration());
            prep.setInt(3, car.getRange());
            prep.setDate(4, Date.valueOf(car.getProductionDate()));
            prep.setString(5, car.getBrand().name());
            prep.setInt(6, car.getId());


            boolean status = prep.executeUpdate() > 0;
            prep.close();
            return status;

        } catch (SQLException throwables) {
            logger.severe(throwables.getMessage());
            return false;
        }
    }

    @Override
    public Car retrieve(String naam) {
        try {
            String statement = "SELECT * FROM cars WHERE naam = ? LIMIT 1";
            PreparedStatement prep = connection.prepareStatement(statement);
            prep.setString(1, naam);
            ResultSet resultSet = prep.executeQuery();
            Car car;
            if (resultSet.next()) {
                car = new Car(
                        resultSet.getInt("id"),
                        resultSet.getString("naam"),
                        resultSet.getDouble("acceleration"),
                        resultSet.getInt("ranges"),
                        resultSet.getDate("productionDate").toLocalDate(),
                        Brand.valueOf(resultSet.getString("brand"))
                );
            } else {
                car = null;
            }
            prep.close();
            resultSet.close();
            return car;

        } catch (SQLException throwables) {
            logger.severe(throwables.getMessage());
        }
        return null;
    }

    @Override
    public List<Car> sortedOn(String query) {
        try {
            PreparedStatement prep = connection.prepareStatement(query);
            ResultSet resultSet = prep.executeQuery();
            List<Car> carList = new ArrayList<>();
            while (resultSet.next()) {
                carList.add(new Car(
                        resultSet.getInt("id"),
                        resultSet.getString("naam"),
                        resultSet.getDouble("acceleration"),
                        resultSet.getInt("ranges"),
                        resultSet.getDate("productionDate").toLocalDate(),
                        Brand.valueOf(resultSet.getString("brand"))
                ));
            }
            prep.close();
            resultSet.close();
            return carList;
        } catch (SQLException throwables) {
            logger.severe(throwables.getMessage());
            return null;
        }
    }
}
