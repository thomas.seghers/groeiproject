package be.kdg.carproject.persist;



import be.kdg.carproject.model.Cars;

import java.io.*;

public class CarSerializer {
    private final String FILENAME;

    public CarSerializer(String FILENAME) {
        this.FILENAME = FILENAME;
    }

    public void serialize(Cars cars) throws IOException {
        ObjectOutputStream oos = new ObjectOutputStream(
                new FileOutputStream("db/cars.ser")
        );
        oos.writeObject(cars);
    }

    public Cars deserialize() throws IOException, ClassNotFoundException {
        FileInputStream fileIn = new FileInputStream("db/cars.ser");
        ObjectInputStream in = new ObjectInputStream(fileIn);
        Cars cars = (Cars) in.readObject();
        in.close();
        fileIn.close();
        return cars;
    }
}
