package be.kdg.carproject.persist;



import be.kdg.carproject.model.Car;

import java.util.List;

public interface CarDao {
    boolean insert(Car car);
    boolean delete(String naam);
    boolean update(Car car);
    Car retrieve(String naam);
    List<Car> sortedOn(String query);

}
