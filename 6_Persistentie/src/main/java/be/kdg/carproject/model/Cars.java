package be.kdg.carproject.model;

import java.io.Serializable;
import java.util.*;

public class Cars implements Serializable {
    private static final long serialVersionUID = 1L;
    TreeSet<Car> cars ;

    public Cars() {
        this.cars = new TreeSet<Car>();
    }

    public boolean add(Car car){
        if (!cars.isEmpty()) {
            for (Car o : cars) {
                if (car.equals(o)) {
                    return false;
                }
            }
        }
        cars.add(car);
        return true;
    }

    public void remove(String name){
        Iterator<Car> it = cars.iterator();
        while (it.hasNext()) {
            if(it.next().getName() == name){
                it.remove();
            }
        }
    }

    public Car search (String name) {
        for (Car o : cars) {
            if (o.name.equals(name)) {
                return o;
            }
        }
        return null;
    }

    public List<Car> sortedOnName(){
        List<Car> sortedList = new ArrayList<>(cars);
        sortedList.sort(Car::compareTo);
        return sortedList;
    }

    public List<Car> sortedOnDate(){
        List<Car> sortedList = new ArrayList<>(cars);
        sortedList.sort(new Comparator<Car>() {
            @Override
            public int compare(Car o1, Car o2) {
                return o1.productionDate.compareTo(o2.productionDate);
            }
        });
        return sortedList;
    }

    public List<Car> sortedOnAcceleration(){
        List<Car> sortedList = new ArrayList<>(cars);
        sortedList.sort(new Comparator<Car>() {
            @Override
            public int compare(Car o1, Car o2) {
                return Double.compare(o1.acceleration,o2.acceleration);
            }
        });
        return sortedList;
    }

    public int getSize(){
        return cars.size();
    }
    public Set<Car> getCars() {
        return cars;
    }
    @Override
    public boolean equals(Object o) {
        if(o instanceof Cars){
            return cars.equals(((Cars) o).getCars());
        }
        return false;
    }
}
