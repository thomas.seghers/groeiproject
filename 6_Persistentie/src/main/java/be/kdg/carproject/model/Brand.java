package be.kdg.carproject.model;

public enum Brand {
    BMW("BMW"), TESLA("Tesla"), AUDI("Audi"), MERCEDES("Mercedes Benz"), JAGUAR("Jaguar");

    private String name;

    private Brand(String name) {
        this.name = name;
    }

    public boolean equalsName(String otherName) {
        return name.equals(otherName);
    }

    @Override
    public String toString() {
        return this.name;
    }
}
