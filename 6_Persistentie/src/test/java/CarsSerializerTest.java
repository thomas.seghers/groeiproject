import be.kdg.carproject.data.Data;
import be.kdg.carproject.model.Car;
import be.kdg.carproject.model.Cars;
import be.kdg.carproject.persist.CarSerializer;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

public class CarsSerializerTest {
    private CarSerializer serializer;
    private Cars cars;

    @BeforeEach
    void setUp() {
        cars = new Cars();
        Data.getData().forEach(car -> cars.add(car));
        serializer = new CarSerializer("db/cars.ser");
    }

    @Test
    public void testSerialize(){
        assertDoesNotThrow(() -> serializer.serialize(cars), "Serialize exception has been thrown");
    }

    @Test
    public void testDeserialize(){
        try{
            Cars cars1 = serializer.deserialize();
            assertEquals(cars, cars1, "Auto's zijn niet hetzelfde!");
        } catch (IOException | ClassNotFoundException e) {
            fail(e);
        }
    }
}
