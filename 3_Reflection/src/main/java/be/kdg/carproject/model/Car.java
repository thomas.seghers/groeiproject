package be.kdg.carproject.model;

import java.time.LocalDate;
import java.util.Objects;

public class Car extends Vehicle {
    double acceleration;
    int range;

    public Car(String name, double acceleration, int range, LocalDate productionDate, Brand brand) {
        super(name,productionDate,brand);
        setAcceleration(acceleration);
        setRange(range);
    }

    public Car (){
        this("Anoniem",19.0,1,LocalDate.of(1800,1,1),Brand.BMW);
    }

    public void setAcceleration(double acceleration) {
        if (acceleration >0 && acceleration <20){
            this.acceleration = acceleration;
        }else {
            throw new IllegalArgumentException("Acceleration is not realistic") ;
        }

    }

    public void setRange(int range) {
        if (range >0 && range <2000){
            this.range = range;
        }else {
            throw new IllegalArgumentException("the Action range is out of range");
        }

    }

    public double getAcceleration() {
        return acceleration;
    }

    public int getRange() {
        return range;
    }



    @Override
    public String toString() {
        return String.format("%sAcceleration(0-100Km): %2.1f     range: %-4dKm",super.toString(),acceleration, range);
    }
}


