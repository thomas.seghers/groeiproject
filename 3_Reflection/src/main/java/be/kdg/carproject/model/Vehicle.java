package be.kdg.carproject.model;

import be.kdg.carproject.reflection.CanRun;

import java.time.LocalDate;
import java.time.chrono.ChronoLocalDate;
import java.util.Objects;

public class Vehicle implements Comparable<Car> {
    String name;
    LocalDate productionDate;
    Brand brand;

    public Vehicle(){
      this.name = "naam";
      this.brand = Brand.BMW;
      this.productionDate = LocalDate.of(1999,1,1);
    }

    public Vehicle(String name, LocalDate productionDate, Brand brand) {
        this.name = name;
        this.productionDate = productionDate;
        this.brand = brand;
    }

    @CanRun //zonder parameter dus default "dummy"
    public void setName(String name) {
        if (!name.isEmpty()) {
            this.name = name;
        } else {
            throw new IllegalArgumentException("Naam is empty");
        }

    }

    public void setProductionDate(LocalDate productionDate) {
        if (LocalDate.now().isAfter(productionDate)) {
            this.productionDate = productionDate;
        } else {
            throw new IllegalArgumentException("No Realistic date");
        }

    }

    public void setBrand(Brand brand) {
        this.brand = brand;
    }

    public String getName() {
        return name;
    }

    public Brand getBrand() {
        return brand;
    }

    public LocalDate getProductionDate() {
        return productionDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Car)) return false;
        Car car = (Car) o;
        return name.equals(car.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    @Override
    public int compareTo(Car o) {
        return this.name.compareTo(o.name);
    }

    @Override
    public String toString() {
        return String.format("%-14s%-41s (°%4d)   ", this.brand.toString(), this.name, this.productionDate.getYear());
    }
}
