package be.kdg.carproject.reflection;

public @interface CanRun {
    String value() default "dummy";
}
