package be.kdg.carproject.reflection;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Type;

public class ReflectionTools {
    public static void classAnalysis(Class... aClasses) {

        for (Class aClass : aClasses) {

            System.out.printf("\nAnalyse van de klasse: %s\n", aClass.getSimpleName());
            System.out.println("===============================================");
            System.out.printf("Fully qualified name   : %s\n", aClass.getName());
            System.out.printf("Naam van de superklasse: %s\n", aClass.getSuperclass().getSimpleName());
            System.out.printf("Naam van de package    : %s\n", aClass.getPackage());
            System.out.print("Interfaces: ");
            for (int i = 0; i < aClass.getInterfaces().length; i++) {
                System.out.printf("%s ", aClass.getInterfaces()[i].getSimpleName());
            }
            System.out.print("\nConstructors:");
            for (int i = 0; i < aClass.getConstructors().length; i++) {
                System.out.printf("\n%s ", aClass.getConstructors()[i].toGenericString());

            }
            System.out.print("\nattributen     : ");
            for (int i = 0; i < aClass.getDeclaredFields().length; i++) {
                System.out.printf("%s(%s) ", aClass.getDeclaredFields()[i].getName(), aClass.getDeclaredFields()[i].getType().getSimpleName());

            }

            System.out.print("\ngetters        : ");
            for (int i = 0; i < aClass.getDeclaredMethods().length; i++) {
                if (aClass.getDeclaredMethods()[i].getName().contains("get")) {
                    System.out.printf("%s ", aClass.getDeclaredMethods()[i].getName());
                }
            }

            System.out.print("\nsetters        : ");
            for (int i = 0; i < aClass.getDeclaredMethods().length; i++) {
                if (aClass.getDeclaredMethods()[i].getName().contains("set")) {
                    System.out.printf("%s ", aClass.getDeclaredMethods()[i].getName());
                }
            }
            System.out.print("\nandere methoden: ");
            for (int i = 0; i < aClass.getDeclaredMethods().length; i++) {
                if (!(aClass.getDeclaredMethods()[i].getName().contains("set"))&& !(aClass.getDeclaredMethods()[i].getName().contains("get"))) {
                    System.out.printf("%s ", aClass.getDeclaredMethods()[i].getName());
                }
            }
            System.out.println("");

        }

    }

    public static Object runAnnotated (Class aClass){

        try {
            Object obj = aClass.getDeclaredConstructor().newInstance();
            for (Method method : aClass.getMethods()) {
                CanRun annotation = method.getAnnotation(CanRun.class);
                Type[] genericParameterTypes = method.getGenericParameterTypes();
                if (annotation != null && genericParameterTypes.length == 1 && genericParameterTypes[0].getTypeName().equals("java.lang.String")) {
                    method.invoke(obj, annotation.value());
                }
            }
            return obj;
        } catch (InvocationTargetException | InstantiationException | IllegalAccessException | NoSuchMethodException e) {
            e.printStackTrace();
        }


        return null;
    }

}


