package be.kdg.carproject;

import be.kdg.carproject.model.Brand;
import be.kdg.carproject.model.Car;
import be.kdg.carproject.model.Cars;
import be.kdg.carproject.model.Vehicle;

import java.time.LocalDate;

import static be.kdg.carproject.reflection.ReflectionTools.classAnalysis;
import static be.kdg.carproject.reflection.ReflectionTools.runAnnotated;

public class Demo_3 {
    public static void main(String[] args) {

        classAnalysis(Vehicle.class,Car.class,Cars.class);
        System.out.printf("Object aangemaakt:\n %s", runAnnotated(Vehicle.class));

    }
}
