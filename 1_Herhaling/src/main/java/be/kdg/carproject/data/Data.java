package be.kdg.carproject.data;

import be.kdg.carproject.model.Brand;
import be.kdg.carproject.model.Car;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Data {

   public static List<Car> getData(){
        List<Car> data = new ArrayList<>();
        data.add(new Car("Model S Standard Range",4.2,450, LocalDate.of(2019,1,1), Brand.TESLA));
        data.add(new Car("Model X 100D Performance",3.7,542, LocalDate.of(2019,1,1), Brand.TESLA));
        data.add(new Car("Model Y Long Range AWD",5.1,507, LocalDate.of(2021,1,1), Brand.TESLA));
        data.add(new Car("E71 X6 xDrive35i",6.7,765, LocalDate.of(2008,1,1), Brand.BMW));
        data.add(new Car("G15 8 Series Coupe M850i xDrive",3.7,701, LocalDate.of(2018,1,1), Brand.BMW));
        data.add(new Car("G11 7 Series 740i",5.5,1181, LocalDate.of(2015,1,1), Brand.BMW));
        data.add(new Car("i4 eDrive40",5.7,590, LocalDate.of(2021,1,1), Brand.BMW));
        data.add(new Car("GLC Coupe (C253 2020) 200 4MATIC",8.0,929, LocalDate.of(2019,1,1), Brand.MERCEDES));
        data.add(new Car("E Class All Terrain (W213) 220 d 4MATIC",8.0,1269 , LocalDate.of(2016,1,1), Brand.MERCEDES));
        data.add(new Car("EQS 580 4MATIC AMG Line",4.3,652, LocalDate.of(2021,1,1), Brand.MERCEDES));
        data.add(new Car("AMG GT 4-door 63 S 4MATIC+",3.2,707, LocalDate.of(2018,1,1), Brand.MERCEDES));
        data.add(new Car("F-Pace 2021 P400",5.4,921, LocalDate.of(2020,1,1), Brand.JAGUAR));
        data.add(new Car("XE 2.0 i4 240HP Portfolio Auto",6.8,840, LocalDate.of(2014,1,1), Brand.JAGUAR));
        data.add(new Car("XF (X260) 2.0 GTDi 240HP Auto Portfolio",7.0,986, LocalDate.of(2015,1,1), Brand.JAGUAR));
        data.add(new Car("F-Type 2018 SVR V8 AWD 575HP",3.7,619, LocalDate.of(2017,1,1), Brand.JAGUAR));

        return data;

    }
}
