package be.kdg.carproject;

import be.kdg.carproject.data.Data;
import be.kdg.carproject.model.Brand;
import be.kdg.carproject.model.Car;
import be.kdg.carproject.model.Cars;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.jar.JarEntry;

public class Demo_1 {
    public static void main(String[] args) {
        Cars cars = new Cars();

        List<Car> data = Data.getData();

        for (Car o : data) {
            cars.add(o);
        }

        if (!cars.add(new Car("Model X 100D Performance",3.7,542, LocalDate.of(2019,1,1), Brand.TESLA))){
            System.out.println("Deze auto zit al in de lijst");
        }

        System.out.println(cars.search("F-Pace 2021 P400").toString());
        cars.remove("GLC Coupe (C253 2020) 200 4MATIC");
        System.out.printf("De de lijst bevat %d elementen.",cars.getSize());

        System.out.println("\nGesorteerd op naam:");
        for (Car o : cars.sortedOnName()) {
            System.out.println(o.toString());
        }

        System.out.println("\nGesorteerd op datum:");
        for (Car o : cars.sortedOnDate()) {
            System.out.println(o.toString());
        }

        System.out.println("\nGesorteerd op Acceleration:");
        for (Car o : cars.sortedOnAcceleration()) {
            System.out.println(o.toString());
        }

        try {new Car("",3.7,542, LocalDate.of(2019,1,1), Brand.TESLA);

        }catch (IllegalArgumentException i){
            System.out.println(i.getMessage());
        }
        try {new Car("Model X 100D Performance",30,542, LocalDate.of(2019,1,1), Brand.TESLA);

        }catch (IllegalArgumentException i){
            System.out.println(i.getMessage());
        }
        try {new Car("Model X 100D Performance",3.7,23000, LocalDate.of(2019,1,1), Brand.TESLA);

        }catch (IllegalArgumentException i){
            System.out.println(i.getMessage());
        }
        try {new Car("Model X 100D Performance",3.7,542, LocalDate.of(2025,1,1), Brand.TESLA);

        }catch (IllegalArgumentException i){
            System.out.println(i.getMessage());
        }

        System.out.println(new Car().toString());

    }
}
