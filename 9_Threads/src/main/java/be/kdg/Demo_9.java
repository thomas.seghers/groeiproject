package be.kdg;

import be.kdg.carproject.data.Data;
import be.kdg.carproject.model.Brand;
import be.kdg.carproject.model.Car;
import be.kdg.carproject.model.Cars;
import be.kdg.carproject.threading.CarRunnable;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Demo_9 {
    private static int TESTCOUNT = 1;
    public static void main(String[] args) {

        long test;
        List<Long> threadTime = new ArrayList<Long>();
        while(TESTCOUNT < 101){
            test = System.currentTimeMillis();

            CarRunnable CC1 = new CarRunnable(r -> r.getRange() < 600);
            CarRunnable CC2 = new CarRunnable(r -> r.getProductionDate().isBefore(LocalDate.of(1500, 1, 1)));
            CarRunnable CC3 = new CarRunnable(r -> r.getName().startsWith("b"));

            Thread thread1 = new Thread(CC1, "thread1");
            Thread thread2 = new Thread(CC2, "thread2");
            Thread thread3 = new Thread(CC3, "thread3");

            thread1.setDaemon(true);
            thread2.setDaemon(true);
            thread3.setDaemon(true);

            thread1.start();
            thread2.start();
            thread3.start();

            try{
                thread1.join();
                thread2.join();
                thread3.join();

                CC1.getCarenList().stream().limit(5).forEach(System.out::println);
                CC2.getCarenList().stream().limit(5).forEach(System.out::println);
                CC3.getCarenList().stream().limit(5).forEach(System.out::println);

                TESTCOUNT++;
                threadTime.add(System.currentTimeMillis()-test);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println("3 threads verzamelen elk 1000 auto's (gemiddelde uit 100 runs): " + threadTime.stream().mapToDouble(Long::doubleValue).average().getAsDouble() + "ms");
    }
}
