package be.kdg;

import be.kdg.carproject.model.Car;
import be.kdg.carproject.threading.CarCallable;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

public class Demo_11 {
    private static int TESTCOUNT = 1;

    public static void main(String[] args) {
        ExecutorService executor = Executors.newFixedThreadPool(3);

        long test;
        List<Long> threadTime = new ArrayList<Long>();
        while (TESTCOUNT < 101) {
            test = System.currentTimeMillis();

            Callable<List<Car>> call1 = new CarCallable(r -> r.getRange() < 600);
            Callable<List<Car>> call2 = new CarCallable(r -> r.getProductionDate().isBefore(LocalDate.of(1500, 1, 1)));
            Callable<List<Car>> call3 = new CarCallable(r -> r.getName().startsWith("b"));

            executor.submit(call1);
            executor.submit(call2);
            executor.submit(call3);

            Future<List<Car>> future1 = executor.submit(call1);
            Future<List<Car>> future2 = executor.submit(call2);
            Future<List<Car>> future3 = executor.submit(call3);

            while (!future1.isDone() && !future2.isDone() && !future3.isDone()) {
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            try {
                System.out.println("Future 1 " + future1.get());
                System.out.println("Future 2 " + future2.get());
                System.out.println("Future 3 " + future3.get());
                executor.awaitTermination(0, TimeUnit.MINUTES);
                TESTCOUNT++;
                threadTime.add(System.currentTimeMillis() - test);
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        }
        executor.shutdown();
        System.out.println();
        System.out.println("3 Futures verzamelen elk 1000 auto's (gemiddelde uit 100 runs): " + threadTime.stream().mapToDouble(Long::doubleValue).average().getAsDouble() + "ms");
    }
}
