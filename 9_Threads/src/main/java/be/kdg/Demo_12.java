package be.kdg;

import be.kdg.carproject.model.CarFactory;
import be.kdg.carproject.model.Cars;

import java.util.stream.Stream;

public class Demo_12 {
    public static void main(String[] args) {
        long begin = System.currentTimeMillis();
        Cars cars = new Cars(10000);

        Thread thread1 = new Thread(() -> Stream.generate(CarFactory::newRandomCar).limit(5000).forEach(cars::add));
        Thread thread2 = new Thread(() -> Stream.generate(CarFactory::newRandomCar).limit(5000).forEach(cars::add));

        thread1.setDaemon(true);
        thread2.setDaemon(true);
        thread1.start();
        thread2.start();
        try{
            thread1.join();
            thread2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Na toevoegen door 2 threads met elk 5000 objecten: cars = " + cars.getSize());
    }
}
