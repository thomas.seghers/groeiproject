package be.kdg.carproject.model;

import java.time.LocalDate;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class CarFactory {
    private CarFactory() {

    }

    public static Car newEmptyCar(){
        return new Car();
    }

    public static Car newFilledCar(String name,double acceleration, int range,  LocalDate productiondate, Brand brand){
        return new Car(name,  acceleration,range,  productiondate, brand);
    }

    public static Car newRandomCar(){
        Random random = new Random();
        int range = random.nextInt(1999) + 1;
        String name = generateString(15);
        double acceleration = 20 * random.nextDouble();
        LocalDate productiondate = generateDate();
        Brand brand = Brand.getRandomBrand();

        return new Car(name, acceleration,range, productiondate, brand);

    }

    private static LocalDate generateDate() {
        long minDay = LocalDate.of(1000, 1, 1).toEpochDay();
        long maxDay = LocalDate.now().toEpochDay();
        long randomDay = ThreadLocalRandom.current().nextLong(minDay, maxDay);
        LocalDate productiondate = LocalDate.ofEpochDay(randomDay);
        return productiondate;
    }

    private static String generateString(int length) {

        int leftLimit = 97; // letter 'a'
        int rightLimit = 122; // letter 'z'
        int targetStringLength = length;
        Random random = new Random();
        String buffer = IntStream.range(0, targetStringLength).map(i -> leftLimit + (int)
                (random.nextFloat() * (rightLimit - leftLimit + 1))).mapToObj(randomLimitedInt -> String.valueOf((char) randomLimitedInt)).collect(Collectors.joining());
        String generatedString = buffer;
        return generatedString;
    }
}
