package be.kdg.carproject.model;

import java.util.*;
import java.util.concurrent.ArrayBlockingQueue;

public class Cars {
//    TreeSet<Car> cars ;
//
//    public Cars() {
//        this.cars = new TreeSet<Car>();
//    }

    private ArrayBlockingQueue<Car> cars;

    public Cars(int capacity) {
        this.cars = new ArrayBlockingQueue<>(capacity);
    }

    public boolean add(Car car) {
        if (!cars.isEmpty()) {
            for (Car o : cars) {
                if (car.equals(o)) {
                    return false;
                }
            }
        }
        cars.add(car);
        return true;
    }

    public void remove(String name) {
        Iterator<Car> it = cars.iterator();
        while (it.hasNext()) {
            if (it.next().getName() == name) {
                it.remove();
            }
        }
    }

    public Car search(String name) {
        return cars.stream().filter(next -> next.getName().equals(name)).findFirst().orElse(null);
    }

    public List<Car> sortedOnName() {
        List<Car> sortedList = new ArrayList<>(cars);
        sortedList.sort(Car::compareTo);
        return sortedList;
    }

    public List<Car> sortedOnDate() {
        List<Car> sortedList = new ArrayList<>(cars);
        sortedList.sort(new Comparator<Car>() {
            @Override
            public int compare(Car o1, Car o2) {
                return o1.getProductionDate().compareTo(o2.getProductionDate());
            }
        });
        return sortedList;
    }

    public List<Car> sortedOnAcceleration() {
        List<Car> sortedList = new ArrayList<>(cars);
        sortedList.sort(new Comparator<Car>() {
            @Override
            public int compare(Car o1, Car o2) {
                return Double.compare(o1.getAcceleration(), o2.getAcceleration());
            }
        });
        return sortedList;
    }

    public int getSize() {
        return cars.size();
    }
}
