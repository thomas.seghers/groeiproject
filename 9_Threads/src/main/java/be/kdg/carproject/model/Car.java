package be.kdg.carproject.model;

import java.time.LocalDate;
import java.util.Objects;

public final class Car implements Comparable<Car>{
   private final String name;
   private final double acceleration;
   private final int range;
   private final LocalDate productionDate;
   private final Brand brand;

    public Car(String name, double acceleration, int range, LocalDate productionDate, Brand brand) {
       this.name=name;
       this.acceleration = acceleration;
       this.range = range;
       this.productionDate = productionDate;
       this.brand = brand;
    }

    public Car (){
        this("Anoniem",19.0,1,LocalDate.of(1800,1,1),Brand.BMW);
    }



    public String getName() {
        return name;
    }

    public double getAcceleration() {
        return acceleration;
    }

    public int getRange() {
        return range;
    }

    public LocalDate getProductionDate() {
        return productionDate;
    }

    public Brand getBrand() {
        return brand;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Car)) return false;
        Car car = (Car) o;
        return name.equals(car.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    @Override
    public int compareTo(Car o) {
        return this.name.compareTo(o.name) ;
    }

    @Override
    public String toString() {
        return String.format("%-14s%-41s (°%4d)   Acceleration(0-100Km): %2.1f     range: %-4dKm",this.brand.toString(),this.name,this.productionDate.getYear(),acceleration, range);
    }
}


