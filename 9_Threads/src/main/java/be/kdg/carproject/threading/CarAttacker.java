package be.kdg.carproject.threading;

import be.kdg.carproject.model.Car;

import java.util.List;
import java.util.function.Predicate;

public class CarAttacker implements Runnable {
    private List<Car> items;
    private Predicate<Car> predicate;

    public CarAttacker(List<Car> items, Predicate<Car> predicate) {
        this.items = items;
        this.predicate = predicate;
    }

    @Override
    public void run() {
        synchronized (items) {
            items.removeIf(predicate);
        }
    }
}
