package be.kdg.carproject.threading;

import be.kdg.carproject.model.Car;
import be.kdg.carproject.model.CarFactory;

import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CarRunnable implements Runnable {
    private Predicate<Car> predicate;
    private List<Car> carsList;

    public CarRunnable() {
    }

    public CarRunnable(Predicate<Car> predicate) {
        this.predicate = predicate;
    }

    @Override
    public void run() {
        this.carsList = Stream.generate(CarFactory::newRandomCar).filter(predicate).limit(1000).collect(Collectors.toList());
    }

    public List<Car> getCarenList() {
        return carsList;
    }
}
