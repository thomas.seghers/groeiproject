package be.kdg.carproject.threading;

import be.kdg.carproject.model.Car;
import be.kdg.carproject.model.CarFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CarCallable implements Callable {
    private List<Car> rijkList = new ArrayList<>();
    private final Predicate<Car> predicate;

    public CarCallable(Predicate<Car> predicate) {
        this.predicate = predicate;
    }

    @Override
    public List<Car> call() throws Exception {
        return Stream.generate(CarFactory::newRandomCar).filter(predicate).limit(1000).collect(Collectors.toList());
    }
}
