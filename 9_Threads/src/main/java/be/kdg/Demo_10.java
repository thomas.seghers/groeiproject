package be.kdg;

import be.kdg.carproject.model.Car;
import be.kdg.carproject.model.CarFactory;
import be.kdg.carproject.threading.CarAttacker;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Demo_10 {
    public static void main(String[] args) {
        List<Car> carList = Stream.generate(CarFactory::newRandomCar).limit(1000).collect(Collectors.toList());
        Thread thread1 = new Thread(new CarAttacker(carList, r -> r.getRange() < 600), "thread1");
        Thread thread2 = new Thread(new CarAttacker(carList, r -> r.getProductionDate().isBefore(LocalDate.of(1500, 1, 1))), "thread2");
        Thread thread3 = new Thread(new CarAttacker(carList, r -> r.getName().startsWith("b")), "thread3");

        thread1.setDaemon(true);
        thread2.setDaemon(true);
        thread3.setDaemon(true);

        thread1.start();
        thread2.start();
        thread3.start();

        try {
            thread1.join();
            thread2.join();
            thread3.join();
            System.out.println("Na uitzuivering:");
            System.out.println("Aantal auto's met een bereik < 600: " + carList.stream().filter(r -> r.getRange() < 600).count());
            System.out.println("Aantal producties voor 1500: " + carList.stream().filter(r -> r.getProductionDate().isBefore(LocalDate.of(1500, 1, 1))).count());
            System.out.println("Aantal autonamen die starten met 'b': " + carList.stream().filter(r -> r.getName().startsWith("b")).count());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
