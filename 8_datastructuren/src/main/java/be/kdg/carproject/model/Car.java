package be.kdg.carproject.model;

import java.time.LocalDate;
import java.util.Objects;

public class Car implements Comparable<Car>{
    String name;
    double acceleration;
    int range;
    LocalDate productionDate;
    Brand brand;
    public static int compareCounter = 0;
    public static int equalsCounter = 0;

    public Car(String name, double acceleration, int range, LocalDate productionDate, Brand brand) {
        setName(name);
        setAcceleration(acceleration);
        setRange(range);
        setProductionDate(productionDate);
        setBrand(brand);
    }

    public Car (){
        this("Anoniem",19.0,1,LocalDate.of(1800,1,1),Brand.BMW);
    }

    public void setName(String name) {
        if(!name.isEmpty()){
           this.name = name;
        }else {
            throw new IllegalArgumentException("Naam is empty") ;
        }

    }

    public void setAcceleration(double acceleration) {
        if (acceleration >0 && acceleration <20){
            this.acceleration = acceleration;
        }else {
            throw new IllegalArgumentException("Acceleration is not realistic") ;
        }

    }

    public void setRange(int range) {
        if (range >0 && range <2000){
            this.range = range;
        }else {
            throw new IllegalArgumentException("the Action range is out of range");
        }

    }

    public void setProductionDate(LocalDate productionDate) {
        if (LocalDate.now().isAfter(productionDate)){
            this.productionDate = productionDate;
        }else {
            throw new IllegalArgumentException("No Realistic date");
        }

    }

    public void setBrand(Brand brand) {
        this.brand = brand;
    }

    public String getName() {
        return name;
    }

    public double getAcceleration() {
        return acceleration;
    }

    public int getRange() {
        return range;
    }

    public LocalDate getProductionDate() {
        return productionDate;
    }

    public Brand getBrand() {
        return brand;
    }

    @Override
    public boolean equals(Object o) {
        equalsCounter++;
        if (this == o) return true;
        if (!(o instanceof Car)) return false;
        Car car = (Car) o;
        return name.equals(car.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    @Override
    public int compareTo(Car o) {compareCounter++; return this.name.compareTo(o.name) ;
    }

    @Override
    public String toString() {
        return String.format("%-14s%-41s (°%4d)   Acceleration(0-100Km): %2.1f     range: %-4dKm",this.brand.toString(),this.name,this.productionDate.getYear(),acceleration, range);
    }
}


