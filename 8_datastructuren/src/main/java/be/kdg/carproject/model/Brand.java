package be.kdg.carproject.model;

import java.util.Random;

public enum Brand {
    BMW("BMW"), TESLA("Tesla"), AUDI("Audi"), MERCEDES("Mercedes Benz"), JAGUAR("Jaguar");

    private String name;

    private Brand(String name) {
        this.name = name;
    }

    public boolean equalsName(String otherName) {
        return name.equals(otherName);
    }

    public static Brand getRandomBrand() {
        Random random = new Random();
        return values()[random.nextInt(values().length)];
    }

    @Override
    public String toString() {
        return this.name;
    }
}
