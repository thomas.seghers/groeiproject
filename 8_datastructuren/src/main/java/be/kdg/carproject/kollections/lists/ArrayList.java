package be.kdg.carproject.kollections.lists;

import be.kdg.carproject.kollections.Kollections;
import be.kdg.carproject.kollections.lists.List;

public class ArrayList<E> implements List<E> {
    private static final int INITIAL_CAPACITY = 10;
    private Object[] elements;
    private int size;

    public ArrayList() {
        elements = new Object[INITIAL_CAPACITY];
        size = 0;
    }
    public ArrayList(int size) {
        elements = new Object[size];
        this.size = 0;
    }

    private void expand() {
        Object [] newElements = new Object[elements.length * 2];
        System.arraycopy(elements, 0, newElements, 0, elements.length);
        elements = newElements;
    }

    @Override
    public void add(int index, E element) {
        if (index > this.size || index < 0) {
            throw new IndexOutOfBoundsException("index: " + index + ", size: " + size);
        }
        if (elements.length == size)
            expand();
        if (index == 0) {
            Object[] tempArr = new Object[elements.length + (size > elements.length ? 0 : 1)];
            System.arraycopy(elements, 0, tempArr, 1, elements.length);
            tempArr[0] = element;
            elements = tempArr;
        } else {
            elements[index] = element;
        }
        size++;
    }

    @Override
    public void add(E element) {
        add(size, element);
    }





    @Override
    public void set(int index, E element) {
        if (index > this.size || index < 0) {
            throw new IndexOutOfBoundsException("index: " + index + ", size: " + size);
        }
        elements[index] = element;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    @SuppressWarnings("unchecked")
    public E remove(int index) {
        if (index >= this.size || index < 0) {
            throw new IndexOutOfBoundsException("index: " + index + ", size: " + size);
        }
        E oldValue = (E) elements[index];
        for (int i = index; i < size - 1; i++) {
            elements[i] = elements[i + 1];
        }
        size--;
        return oldValue;
    }

    @Override
    @SuppressWarnings("unchecked")
    public E get(int index) {
        if (index >= this.size || index < 0) {
            throw new IndexOutOfBoundsException("index: " + index + ", size: " + size);
        }
        return (E) elements[index];
    }

    @Override
    public int indexOf(E element) {
       return Kollections.lineairSearch(this,element);
    }

    @Override
    public boolean remove(E element) {
        if(this.indexOf(element) >-1){
            this.remove(this.indexOf(element));
            return true;
        }

        return false;
    }

    @Override
    public boolean contains(E element) {
        return indexOf(element)!=-1;
    }
}
