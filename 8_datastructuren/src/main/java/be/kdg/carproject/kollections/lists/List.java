package be.kdg.carproject.kollections.lists;

import be.kdg.carproject.kollections.Collection;

public interface List<T> extends Collection<T>{
    void add(int index, T element);
    void add(T element);
    void set(int index, T element);
    int size();
    T remove(int index);
    T get(int index);
    int indexOf(T element);
}
