package be.kdg.carproject.kollections.sets;


import be.kdg.carproject.kollections.Collection;
import be.kdg.carproject.kollections.lists.List;

public interface Set<E> extends Collection<E> {
    List<E> toList();
}
