package be.kdg.carproject.kollections.maps;


import be.kdg.carproject.kollections.Collection;
import be.kdg.carproject.kollections.sets.Set;

public interface Map<K, V> {
    void put(K key, V value);
    V get(K key);
    Collection<V> values();
    Set<K> keySet();
    int size();
}
