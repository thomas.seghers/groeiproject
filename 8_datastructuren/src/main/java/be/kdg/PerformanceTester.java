package be.kdg;

import be.kdg.carproject.kollections.lists.ArrayList;
import be.kdg.carproject.kollections.Kollections;
import be.kdg.carproject.kollections.lists.LinkedList;
import be.kdg.carproject.kollections.lists.List;
import be.kdg.carproject.kollections.maps.HashMap;
import be.kdg.carproject.kollections.maps.ListMap;
import be.kdg.carproject.kollections.maps.Map;
import be.kdg.carproject.kollections.sets.ArraySet;
import be.kdg.carproject.kollections.sets.TreeSet;
import be.kdg.carproject.model.Car;
import be.kdg.carproject.model.CarFactory;


import java.util.Random;
import java.util.stream.IntStream;

public class PerformanceTester {


    public static List<Car> randomList(int n) {
        List<Car> myList = new ArrayList<>();
        new Random().ints(n).forEach(i -> myList.add(CarFactory.newRandomCar()));
        return myList;
    }

    public static void compareArrayListAndLinkedList(int n) {
        List<Car> arrayList = new ArrayList<>();
        List<Car> linkedList = new LinkedList<>();
        //add at beginning
        //arraylist
        long mil = System.currentTimeMillis();
        new Random().ints(n).forEach(i -> arrayList.add(0, CarFactory.newRandomCar()));
        long mil1 = System.currentTimeMillis();
        long diff1 = mil1 - mil;
        System.out.println("Adding "+ n + " to ArrayList: " + diff1 + " ms.");

        //linkedlist
        long mil2 = System.currentTimeMillis();
        new Random().ints(n).forEach(i -> linkedList.add(0, CarFactory.newRandomCar()));
        long mil3 = System.currentTimeMillis();
        long diff2 = mil3 - mil2;
        System.out.println("Adding "+ n + " to LinkedList: " + diff2 + " ms.");

        //get at end
        //arraylist
        long mil4 = System.currentTimeMillis();
        new Random().ints(n).forEach(i -> arrayList.get(n-1));
        long mil5 = System.currentTimeMillis();
        long diff3 = mil5 - mil4;
        System.out.println("Getting "+ n + " from ArrayList: " + diff3 + " ms.");

        //linkedlist
        long mil6 = System.currentTimeMillis();
        new Random().ints(n).forEach(i -> linkedList.get(n-1));
        long mil7 = System.currentTimeMillis();
        long diff4 = mil7 - mil6;
        System.out.println("Getting "+ n + " from LinkedList: " + diff4 + " ms.");
    }

    public static void testSelectionSort() {
        for (int n = 1000; n < 20000; n += 1000){
            Kollections.selectionSort(randomList(n));
            System.out.println(n + ";" + Car.compareCounter);
        }
    }

    public static void testMergeSort() {
        for (int n = 1000; n < 20000; n += 1000){
            Kollections.mergeSort(randomList(n));
            System.out.println(n +";" + Car.compareCounter);
        }
    }

    public static void compareListMapToHashMap(int size){
        ListMap<String, Car> listMap = new ListMap<>();
        HashMap<String, Car> hashMap = new HashMap<>();

        fillMap(listMap, size);
        fillMap(hashMap, size);
        Car.equalsCounter=0;
        long time1 = System.nanoTime();
        IntStream.range(0, size).mapToObj(i -> "Car" + i).forEach(listMap::get);
        long time2 = System.nanoTime();
        System.out.println("Listmap: n = " + size + ", equalscount = " + Car.equalsCounter + ", nanosec: " + (time2-time1));
        Car.equalsCounter=0;
        time1 = System.nanoTime();
        IntStream.range(0, size).mapToObj(i -> "Car" + i).forEach(hashMap::get);
       time2 = System.nanoTime();
        System.out.println("Hashmap: n = " + size + ", equalscount = " + Car.equalsCounter + ", nanosec: " + (time2-time1));
    }

    private static void fillMap(Map<String, Car> map, int n){
        Car car = CarFactory.newRandomCar();
        car.setName("Car" + n);
        map.put("car", car);
    }

    public static void compareArraySetToTreeSet(int n){
        ArraySet<Car> array = new ArraySet<>();
        TreeSet<Car> tree = new TreeSet<>();

        Car.equalsCounter = 0;
        Car.compareCounter = 0;

        long time1 = System.nanoTime();
        IntStream.range(0, n).forEach(i -> IntStream.range(0, n)
                .mapToObj(j -> CarFactory.newRandomCar()).forEach(array::add));
        System.out.printf("\nArrayset, n = %d: equalsCount: %d", n, Car.equalsCounter);
        System.out.printf("\nArrayset, n = %d: compareCount: %d", n, Car.compareCounter);
        System.out.printf("\nArrayset, n = %d: nanoSec: %d", n, System.nanoTime() - time1);

        Car.equalsCounter = 0;
        Car.compareCounter = 0;

        time1 = System.nanoTime();
        IntStream.range(0, n).forEach(i -> IntStream.range(0, n)
                .mapToObj(j -> CarFactory.newRandomCar()).forEach(tree::add));
        System.out.printf("\nTreeSet, n = %d: equalsCount: %d", n, Car.equalsCounter);
        System.out.printf("\nTreeSet, n = %d: compareCount: %d", n, Car.compareCounter);
        System.out.printf("\nTreeSet, n = %d: nanoSec: %d", n, System.nanoTime() - time1);
    }
}
