package be.kdg;

import be.kdg.carproject.kollections.Kollections;
import be.kdg.carproject.kollections.lists.List;
import be.kdg.carproject.model.Car;
import be.kdg.carproject.model.CarFactory;

import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Demo_8 {
    public static void main(String[] args) {

        Stream.generate(() -> CarFactory.newRandomCar()).limit(30).sorted()
        .forEach(e -> System.out.println(e));
        System.out.println("======================================");
        System.out.println("PreformanceTester");
        System.out.println("======================================");
        List<Car> carList =  PerformanceTester.randomList(10);
       IntStream.range(0, carList.size()).mapToObj(carList::get).forEach(System.out::println);
        System.out.println("======================================");
        System.out.println("Compare ArrayList and Linked List (2000)");
        System.out.println("======================================");
        PerformanceTester.compareArrayListAndLinkedList(20000);


        System.out.println("======================================");
        System.out.println("testSelectioSort");
        System.out.println("======================================");
        PerformanceTester.testSelectionSort();
        System.out.println("======================================");
        System.out.println("testMergeSort");
        System.out.println("======================================");
        PerformanceTester.testMergeSort();


        System.out.println("======================================");
        System.out.println("Test SelectionSort");
        System.out.println("======================================");
        carList = PerformanceTester.randomList(30);
        Kollections.selectionSort(carList);
        IntStream.range(0, carList.size()).mapToObj(carList::get).forEach(System.out::println);
        System.out.println("======================================");
        System.out.println("Test mergeSort");
        System.out.println("======================================");
        carList = PerformanceTester.randomList(30);
        Kollections.mergeSort(carList);
        IntStream.range(0, carList.size()).mapToObj(carList::get).forEach(System.out::println);
        System.out.println("======================================");
        System.out.println("Test QuickSort");
        System.out.println("======================================");
        carList = PerformanceTester.randomList(30);
        Kollections.quickSort(carList);
        IntStream.range(0, carList.size()).mapToObj(carList::get).forEach(System.out::println);

        System.out.println("======================================");
        System.out.println("Test Linear en Binary search");
        System.out.println("======================================");
        List<Car>carList1 =  PerformanceTester.randomList(30 );
        Kollections.quickSort(carList1);
        Car car = carList1.get(7);
        Car carEmpty = CarFactory.newEmptyCar();
        System.out.println("Index of car " + car.getName() + ": " + Kollections.lineairSearch(carList1, car));
        System.out.println("Index of car " + car.getName() + ": " + Kollections.binarySearch(carList1, car));

        System.out.println("Index of carEmpty " + carEmpty.getName() + ": " + Kollections.lineairSearch(carList1, carEmpty));
        System.out.println("Index of carEmpty " + carEmpty.getName() + ": " + Kollections.binarySearch(carList1, carEmpty));

     System.out.println("======================================");
     System.out.println("Compare ListMap to HashMap");
     System.out.println("======================================");
     PerformanceTester.compareListMapToHashMap(1000);

     System.out.println("======================================");
     System.out.println("Compare ArraySet to TreeSet");
     System.out.println("======================================");
     PerformanceTester.compareArraySetToTreeSet(100);


    }
}
